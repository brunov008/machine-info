import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from './exception/http-exception.filter';
import { BadExceptionFilter } from './exception/bad-request-exception.filter';
import { ConstraintExceptionFilter } from './exception/constraint-exception.filter';
import { DuplicateErrorExceptionFilter } from './exception/duplicate-user-exception.filter';
import { EmailExceptionFilter } from './exception/email-exception.filter';
import { NotFoundExceptionFilter } from './exception/not-found-exception.filter';
import { UnauthorizedExceptionFilter } from './exception/unauthorized-exception.filter';

async function bootstrap() {
  const appOptions = {cors: true}
  const app = await NestFactory.create(AppModule, appOptions);
  app.useGlobalFilters(new HttpExceptionFilter(), new NotFoundExceptionFilter(), new BadExceptionFilter(), new EmailExceptionFilter(), new UnauthorizedExceptionFilter(), new DuplicateErrorExceptionFilter(), new ConstraintExceptionFilter())
  app.useGlobalPipes(new ValidationPipe({
    transform: true,
    whitelist: true,
  }))
  
  await app.listen(4000);
}
bootstrap();
