export interface IpResponse{
    readonly status: string,
    readonly country: string,
    readonly countryCode: string,
    readonly region: string,
    readonly regionName: string,
    readonly city: string,
    readonly zip: string,
    readonly lat: number,
    readonly lon: number,
    readonly timezone: string,
    readonly isp: string,
    readonly org: string,
    readonly as: string,
    readonly query: string
}