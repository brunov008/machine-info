import { IpGeo } from "src/entities/geo.entity";
import { Machine } from "src/entities/machine.entity";

export interface InfoResponse{
    geo: IpGeo,
    machine: Machine
}