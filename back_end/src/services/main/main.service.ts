import { HttpService } from '@nestjs/axios';
import { HttpException, Injectable } from '@nestjs/common';
import { IpResponse } from 'src/models/ip.model';
import { catchError, lastValueFrom } from 'rxjs';
import { MachineRepository } from 'src/repository/machine.repository';
import { IpGeoRepository } from 'src/repository/geo.repository';
import { InfoResponse } from 'src/models/response/info.response';
import { UserRepository } from 'src/repository/user.repository';
import { EntityManager, Transaction, TransactionManager } from 'typeorm';
import { IpGeo } from 'src/entities/geo.entity';
import { Machine } from 'src/entities/machine.entity';
import { REQUESTED_RANGE_NOT_SATISFIABLE } from 'http-status'

@Injectable()
export class MainService {

    constructor(
        private httpService: HttpService,
        private machineRepository: MachineRepository,
        private ipGeoRepository: IpGeoRepository,
        private userRepository: UserRepository
    ){}

    @Transaction()
    async getPublicIP(@TransactionManager() em: EntityManager = null): Promise<InfoResponse>{

        const result = await lastValueFrom(this.httpService.get<IpResponse>('http://ip-api.com/json').pipe(
            catchError(_ => {
                throw new HttpException("Falha na requisição..", REQUESTED_RANGE_NOT_SATISFIABLE)
            })
        ))

        let geoResult: IpGeo
        let machineResult: Machine

        const user = await this.userRepository.findOne({
            where: {
                id: 1
            }
        })

        try{
            geoResult = await this.ipGeoRepository.saveGeo(user,em,result.data)
            machineResult = await this.machineRepository.saveMachine(user, em)

            return {
                geo: geoResult,
                machine: machineResult
            }

        }catch(e){
            console.log(e)
            if(geoResult) await em.recover(geoResult)
            if(machineResult) await em.recover(machineResult)

            throw new HttpException("Ocorreu um erro no banco de dados..", 500)
        }
    }
}
