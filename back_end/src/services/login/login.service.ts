import { HttpException, Injectable } from '@nestjs/common';
import { User } from 'src/entities/user.entity';
import { LoginRequest } from 'src/models/request/login.request';
import { UserRepository } from 'src/repository/user.repository';

@Injectable()
export class LoginService {
    
    constructor(
        private userRepository: UserRepository
    ){}

    async authenticate({email} : LoginRequest) : Promise<User>{

        const user = await this.userRepository.findUserByEmail(email)
  
        if(!user ) throw new HttpException('Usuario não cadastrado.', 404)
  
        return user
      }
}
