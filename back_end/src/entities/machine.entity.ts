import { userInfo } from 'os';
import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany} from 'typeorm';
import { CPU } from './cpu.entity';
import { Network } from './network.entity';
import { UserMachineInfo } from './user-info.entity';
import { User } from './user.entity';

@Entity({ name: "TB_machine"})
export class Machine{
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255, nullable: true})
    arch: string;

    @Column("varchar", { length: 255, nullable: true})
    endianness: string;

    @Column("int", { nullable: true})
    freemem: number

    @Column("varchar", { length: 255, nullable: true})
    hostname: string;

    @Column("varchar", { length: 255, nullable: true})
    version: string;

    @Column("varchar", { length: 255, nullable: true})
    platform: string;

    @Column("int", { nullable: true})
    uptime: number

    @Column("int", { nullable: true})
    totalmem: number

    @Column("varchar", { length: 255, nullable: true})
    type: string;

    @OneToMany(() => CPU, cpu => cpu.machine)
    cpus :CPU[]

    @OneToMany(() => Network, net => net.machine)
    networks :Network[]

    @OneToMany(() => UserMachineInfo, info => info.machine)
    userInfos :UserMachineInfo[]

    @ManyToOne(() => User, user => user.machines, {
        nullable: false,
        cascade: ['update', 'insert', 'soft-remove']
    })
    @JoinColumn({name: "usuario"})
    usuario: User;
}