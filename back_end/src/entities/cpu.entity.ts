import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany} from 'typeorm';
import { Machine } from './machine.entity';
import { Times } from './times.entity';

@Entity({ name: "TB_cpu"})
export class CPU{
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255, nullable: true})
    model: string;

    @Column("int", { nullable: true})
    speed: number

    @OneToMany(() => Times, tm => tm.cpu)
    times :Times[]

    @ManyToOne(() => Machine, mch => mch.cpus, {
        nullable: false,
        cascade: ['update', 'insert', 'soft-remove']
    })
    @JoinColumn({name: "machine"})
    machine: Machine;
}