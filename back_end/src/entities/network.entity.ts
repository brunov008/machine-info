import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import { Machine } from './machine.entity';

@Entity({ name: "TB_network"})
export class Network{
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column("int", { nullable: true})
    model: number

    @Column("int", { nullable: true})
    speed: number

    @Column("int", { nullable: true})
    uptime: number

    @ManyToOne(() => Machine, mch => mch.networks, {
        nullable: false,
        cascade: ['update', 'insert', 'soft-remove']
    })
    @JoinColumn({name: "machine"})
    machine: Machine;
}