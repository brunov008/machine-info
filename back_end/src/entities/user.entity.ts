import {Entity, Column, PrimaryGeneratedColumn, Index, OneToMany} from 'typeorm';
import { IpGeo } from './geo.entity';
import { Machine } from './machine.entity';

@Entity({ name: "TB_usuario"})
export class User{
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255, nullable: false})
    @Index({ unique: true })
    email: string;

    @Column("varchar", { length: 255, nullable: true})
    nome: string;

    @Column("varchar", { length: 255, nullable: true})
    imagem: string;

    @OneToMany(() => Machine, mch => mch.usuario)
    machines :Machine[]

    @OneToMany(() => IpGeo, ipgeo => ipgeo.usuario)
    ipgeos :IpGeo[]
}