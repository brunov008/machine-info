import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import { User } from './user.entity';

@Entity({ name: "TB_ip_geo"})
export class IpGeo{
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255, nullable: true})
    status: string;

    @Column("varchar", { length: 255, nullable: true})
    country: string;

    @Column("varchar", { length: 255, nullable: true})
    countryCode: string;

    @Column("varchar", { length: 255, nullable: true})
    region: string;

    @Column("varchar", { length: 255, nullable: true})
    regionName: string;

    @Column("varchar", { length: 255, nullable: true})
    city: string;

    @Column("varchar", { length: 255, nullable: true})
    zip: string;

    @Column("decimal", { precision: 10, scale: 7, nullable: true})
    lat: number

    @Column("decimal", { precision: 10, scale: 7, nullable: true})
    lon: number

    @Column("varchar", { length: 255, nullable: true})
    timezone: string;

    @Column("varchar", { length: 255, nullable: true})
    isp: string;

    @Column("varchar", { length: 255, nullable: true})
    org: string;

    @Column("varchar", { length: 255, nullable: true})
    as: string;

    @Column("varchar", { length: 255, nullable: true})
    query: string;

    @ManyToOne(() => User, user => user.ipgeos, {
        nullable: false,
        cascade: ['update', 'insert', 'soft-remove']
    })
    @JoinColumn({name: "usuario"})
    usuario: User;
}