import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import { Machine } from './machine.entity';

@Entity({ name: "TB_user_info"})
export class UserMachineInfo{
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255, nullable: true})
    username: string;

    @Column("int", { nullable: true})
    uid: number
    
    @Column("int", { nullable: true})
    gid: number

    @Column("varchar", { length: 255, nullable: true})
    shell: string;

    @Column("varchar", { length: 255, nullable: true})
    homedir: string;

    @ManyToOne(() => Machine, mch => mch.userInfos, {
        nullable: false,
        cascade: ['update', 'insert', 'soft-remove']
    })
    @JoinColumn({name: "machine"})
    machine: Machine;
}