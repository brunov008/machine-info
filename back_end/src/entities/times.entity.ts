import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import { CPU } from './cpu.entity';

@Entity({ name: "TB_time"})
export class Times{
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column("int", { nullable: true})
    user: number

    @Column("int", { nullable: true})
    nice: number

    @Column("int", { nullable: true})
    sys: number

    @Column("int", { nullable: true})
    idle: number

    @Column("int", { nullable: true})
    irq: number

    @ManyToOne(() => CPU, cpu => cpu.times, {
        nullable: false,
        cascade: ['update', 'insert', 'soft-remove']
    })
    @JoinColumn({name: "cpu"})
    cpu: CPU;
}