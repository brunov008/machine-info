import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './controllers/user/user.module';
import { MainModule } from './controllers/main/main.module';
import { LoginModule } from './controllers/login/login.module';
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    LoginModule,
    MainModule,
    UserModule
  ]
})
export class AppModule{}
