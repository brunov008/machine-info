import { IpGeo } from "src/entities/geo.entity";
import { User } from "src/entities/user.entity";
import { IpResponse } from "src/models/ip.model";
import { EntityRepository, Repository, EntityManager } from "typeorm";

@EntityRepository(IpGeo) 
export class IpGeoRepository extends Repository<IpGeo>{
    saveGeo(user: User, em: EntityManager, {as, city, country, countryCode, isp, lat, lon, org, query, region, 
        regionName, status, timezone, zip}: IpResponse
    ): Promise<IpGeo>{
        const value = new IpGeo()
        value.as = as
        value.city = city
        value.country = country
        value.countryCode = countryCode
        value.isp = isp
        value.lat = lat
        value.lon = lon
        value.org = org
        value.query = query
        value.region = region
        value.regionName = regionName
        value.status = status
        value.timezone = timezone
        value.zip = zip
        value.usuario = user

        return em.save<IpGeo>(value)
    }
}