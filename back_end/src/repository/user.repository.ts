import { User } from "src/entities/user.entity";
import { EntityRepository, Repository } from "typeorm";

@EntityRepository(User) 
export class UserRepository extends Repository<User>{
    
    async findUserByEmail (email: string){
        return this.findOne({
            where: {
                email: email.toLowerCase().trim()
            }
        })
    }
}