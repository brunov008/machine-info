import { Machine } from "src/entities/machine.entity";
import { EntityRepository, Repository, EntityManager } from "typeorm";
import {arch, endianness, freemem, hostname, platform, version, uptime, cpus, networkInterfaces,
    totalmem, userInfo, type} from 'os'
import { User } from "src/entities/user.entity";

@EntityRepository(Machine) 
export class MachineRepository extends Repository<Machine>{
    
    saveMachine(user: User, em: EntityManager): Promise<Machine>{
        const listCPU = []

        const value = new Machine()
        value.arch = arch()
        value.endianness = endianness()
        value.freemem = freemem()
        value.hostname = hostname()
        value.platform = platform()
        value.version = version()
        value.uptime = uptime()
        cpus().forEach(item => listCPU.push(item))
        value.cpus = listCPU
        //FIXME value.networks = networkInterfaces()
        value.totalmem = totalmem()
        //FIXME value.userInfos = [...userInfo()]
        value.type = type()
        value.usuario = user

        return em.save<Machine>(value)
    }
}