import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MainService } from 'src/services/main/main.service';
import { MainController } from './main.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MachineRepository } from 'src/repository/machine.repository';
import { IpGeoRepository } from 'src/repository/geo.repository';
import { UserRepository } from 'src/repository/user.repository';

@Module({
    imports: [
        HttpModule.register({
          timeout: 10000,
          maxRedirects: 5,
        }),
        TypeOrmModule.forFeature([
          MachineRepository,
          IpGeoRepository,
          UserRepository
        ])
      ],
      providers: [MainService],
      controllers: [MainController],
})
export class MainModule {}
