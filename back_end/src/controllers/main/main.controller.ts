import { Controller, Get } from '@nestjs/common';
import { InfoResponse } from 'src/models/response/info.response';
import { MainService } from 'src/services/main/main.service';

@Controller('main')
export class MainController {
    
    constructor(
        private mainService: MainService
    ){}

    @Get('info')
    async getInfo(): Promise<InfoResponse>{
        return await this.mainService.getPublicIP()
    }
}
