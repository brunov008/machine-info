import { Module } from '@nestjs/common';
import { UserRepository } from 'src/repository/user.repository';
import { LoginService } from 'src/services/login/login.service';
import { LoginController } from './login.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from 'src/services/user/user.service';

@Module({
  imports: [
      TypeOrmModule.forFeature([
        UserRepository
      ])
    ],
    providers: [LoginService, UserService],
    controllers: [LoginController]
})
export class LoginModule {}
