import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { User } from 'src/entities/user.entity';
import { LoginRequest } from 'src/models/request/login.request';
import { LoginService } from 'src/services/login/login.service';
import { UserService } from 'src/services/user/user.service';

@Controller('login')
export class LoginController { 
    
    constructor(
        private loginService: LoginService,
        private userService: UserService,
    ) {} 

    @Post('auth')
    @HttpCode(200)
    async authenticate(@Body() loginUser: LoginRequest): Promise<User> { 
        return await this.loginService.authenticate(loginUser)
    }
}
