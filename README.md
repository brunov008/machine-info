@@ Em desenvolvimento @@

## Propósito
- Informações gerais de uma maquina

## Configuração Necessárias
- Docker: [https://www.docker.com]

## Rodando a aplicação
- Verifique se o docker esta disponivel nas variaveis com o comando [docker -v] e [docker-compose -v]
- inicie com o comando [docker-compose up] na raiz do projeto

- localhost:3002 react
- localhost:4000 nest (GET localhost:4000/main/info -> esse endpoint é daora)
- localhost:9090 prometheus (Monitoramento)
- localhost:3000 grafana (usuario: admin, senha: admin123)

## Postman Link
- https://www.getpostman.com/collections/a82876464eebb2bae4c7

## Observação:
- Para iniciar todos os container leva em torno de 4 minutos #just chill #take a coffe #relax #go fishing


Enjoy :)

Easter Egg

eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NDYwMjY4MDAsImlhdCI6MTY0NTk0MDQwMCwic3ViIjoibWluaWZ5IHRoaXMgOikgLT4gMDExMDEwMDAgMDExMTAxMDAgMDExMTAxMDAgMDExMTAwMDAgMDExMTAwMTEgMDAxMTEwMTAgMDAxMDExMTEgMDAxMDExMTEgMDExMTAxMTEgMDExMTAxMTEgMDExMTAxMTEgMDAxMDExMTAgMDExMTEwMDEgMDExMDExMTEgMDExMTAxMDEgMDExMTAxMDAgMDExMTAxMDEgMDExMDAwMTAgMDExMDAxMDEgMDAxMDExMTAgMDExMDAwMTEgMDExMDExMTEgMDExMDExMDEgMDAxMDExMTEgMDExMTAxMTEgMDExMDAwMDEgMDExMTAxMDAgMDExMDAwMTEgMDExMDEwMDAgMDAxMTExMTEgMDExMTAxMTAgMDAxMTExMDEgMDEwMDAwMTEgMDEwMTAwMTAgMDEwMDExMTAgMDExMDEwMDEgMDExMDAwMTAgMDExMTAxMDAgMDEwMDAwMTAgMDExMDEwMDAgMDAxMTAxMDAgMDExMTEwMDEgMDEwMTAxMDEgMDAxMDAxMTAgMDExMDExMDAgMDExMDEwMDEgMDExMTAwMTEgMDExMTAxMDAgMDAxMTExMDEgMDEwMDExMDAgMDEwMDExMDAgMDAxMDAxMTAgMDExMDEwMDEgMDExMDExMTAgMDExMDAxMDAgMDExMDAxMDEgMDExMTEwMDAgMDAxMTExMDEgMDAxMTAwMDEgMDAxMTAwMDAgMDAxMTEwMDAifQ.WZFEPuFDJpKTKQn2eOl60gcAvfXTUJnHYB68HBZ82Bg